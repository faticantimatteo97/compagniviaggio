import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';

class UploadScreen extends StatelessWidget {
  const UploadScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Upload'),
        ),
        body: SingleChildScrollView(
            child: Column(
          children: <Widget>[
            Container(
              height: 20,
            ),
            Center(
              child: Container(
                  width: MediaQuery.of(context).size.width - 30,
                  height: 80,
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Inserisci qui il luogo',
                    ),
                  )),
            ),
            Center(
              child: Container(
                  width: MediaQuery.of(context).size.width - 30,
                  height: 100,
                  child: TextField(
                    maxLines: 2,
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Descrizione Sintetica',
                    ),
                  )),
            ),
            Center(
              child: Container(
                  height: 140,
                  width: MediaQuery.of(context).size.width - 30,
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    maxLines: 4,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Descrizione',
                    ),
                  )),
            ),
            Center(child: buildStack())
          ],
        )));
  }

  Widget buildStack() => GestureDetector(
      onTap: () async {
        print("onTap called");
        String uriOfImage = await upload();
        print(Image.network(uriOfImage).image);
      },
      child: Container(
        child: CircleAvatar(
          backgroundImage: AssetImage('assets/plus.png'),
          radius: 40,
        ),
      ));
}

final Firestore firestore = Firestore.instance;

Future<String> upload() async {
  //pick image   use ImageSource.camera for accessing camera.
  File image = await ImagePicker.pickImage(source: ImageSource.gallery);
  //basename() function will give you the filename
  String fileName = basename(image.path);
  //passing your path with the filename to Firebase Storage Reference
  StorageReference reference = FirebaseStorage().ref().child("your_path/$fileName");
  //upload the file to Firebase Storage
  StorageUploadTask uploadTask = reference.putFile(image);
  //Snapshot of the uploading task
  StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
  await firestore.collection('users').document("pictures").setData({'urlpic1': await taskSnapshot.ref.getDownloadURL()});
  return await taskSnapshot.ref.getDownloadURL();
}
