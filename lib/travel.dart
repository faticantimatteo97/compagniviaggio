import 'package:flutter/material.dart';

class Travel extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MyStatefulWidget();
  }
}

class MyStatefulWidget extends StatelessWidget {
  const MyStatefulWidget({Key key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              FlatButton.icon(
                color: Colors.blue[200],
                icon: Icon(Icons.arrow_back), //`Icon` to display
                label: Text('Viaggio'), //`Text` to display
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          )
        ),

    );
  }
}