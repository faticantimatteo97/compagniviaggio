import 'package:flutter/material.dart';
import 'package:flutter_app/home.dart';
import 'package:flutter_app/travel.dart';

var locations = {
  "Ponza": "https://bit.ly/2XPjiL6",
  "Dubai": "https://bit.ly/2KiEIxd",
  "Brasile": "https://bit.ly/2XKfN8O"
};

class Profile extends StatelessWidget {
  const Profile({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Profilo'),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/background.jpg"),
                      fit: BoxFit.fill),
                ),
                child: Center(
                  child: buildStack(),
                )),
            Center(
              child: Container(
                  width: MediaQuery.of(context).size.width - 20,
                  child: TextField(
                    onSubmitted: (newValue) {
                      print(newValue);
                    },
                    textInputAction: TextInputAction.done,
                    maxLines: 3,
                    decoration: InputDecoration(
                      //border: OutlineInputBorder(),
                      labelText: 'Descrizione Profilo',
                    ),
                  )),
            ),
            Flexible(
                child: GridView.count(
                    physics: BouncingScrollPhysics(),
                    crossAxisCount: 2,
                    children:
                        new List<Widget>.generate(locations.length, (index) {
                      return new GridTile(
                          child: GestureDetector(
                              onTap: () {
                                print("onTap called.$index");
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Travel()),
                                );
                              },
                              child: Stack(
                                  alignment: const Alignment(0.6, 0.6),
                                  children: [
                                    Card(
                                      semanticContainer: true,
                                      clipBehavior: Clip.antiAlias,
                                      child: Image.network(
                                        locations.values.toList()[index],
                                        fit: BoxFit.fill,
                                        height: 150,
                                      ),
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(6.0),
                                      ),
                                      margin: EdgeInsets.all(10),
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: Colors.black45,
                                      ),
                                      child: Text(
                                        locations.entries.toList()[index].key,
                                        style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ])));
                    })))
          ],
        ));
  }

  Widget buildStack() => Stack(
        alignment: const Alignment(0.6, 0.6),
        children: [
          CircleAvatar(
            backgroundImage: AssetImage('assets/flutter_logo.png'),
            radius: 80,
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.black45,
            ),
            child: Text(
              currentuser.nickname,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
        ],
      );
}
