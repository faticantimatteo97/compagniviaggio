import 'package:flutter/material.dart';
import 'package:flutter_app/home.dart';
import 'package:flutter_app/profile.dart';
import 'package:flutter_app/uploadscreen.dart';


class BottomNavigationBarController extends StatefulWidget {
  @override
  _BottomNavigationBarControllerState createState() =>
      _BottomNavigationBarControllerState();
}

class _BottomNavigationBarControllerState
    extends State<BottomNavigationBarController> {
  final List<Widget> pages = [
    MyStatefulWidget(
      key: PageStorageKey('Page1'),
    ),
    UploadScreen(
      key: PageStorageKey('Page2'),
    ),
    Profile(
      key: PageStorageKey('Page3'),
    ),
  ];

  final PageStorageBucket bucket = PageStorageBucket();

  int _selectedIndex = 0;

  Widget _bottomNavigationBar(int selectedIndex) => BottomNavigationBar(
        onTap: (int index) => setState(() => _selectedIndex = index),
        currentIndex: selectedIndex,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.home), title: Padding(padding: EdgeInsets.all(0))),
          BottomNavigationBarItem(
              icon: Icon(Icons.add_circle_outline), title: Padding(padding: EdgeInsets.all(0))),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle), title: Padding(padding: EdgeInsets.all(0))),
        ],
        type: BottomNavigationBarType.fixed
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: Drawer(),
        appBar: _selectedIndex == 0 ? AppBar(
          title: const Text('WEROVE'),
          actions: [
            Builder(
              builder: (context) => IconButton(
                icon: Icon(Icons.message),
                onPressed: () => Scaffold.of(context).openEndDrawer(),
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              ),
            ),
          ],
        ) : null,
      bottomNavigationBar: _bottomNavigationBar(_selectedIndex),
      body: PageStorage(
        child: pages[_selectedIndex],
        bucket: bucket,
      ),
    );
  }
}

