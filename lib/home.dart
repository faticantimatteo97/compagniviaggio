import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/authentication_bloc/authentication_bloc.dart';
import 'package:flutter_app/authentication_bloc/authentication_event.dart';
import 'package:flutter_app/navigation_bar_controller.dart';
import 'package:flutter_app/users.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

User currentuser;

class HomeScreen extends StatelessWidget {

  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    createUser();
    return BottomNavigationBarController();
  }

  createUser() async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    FirebaseUser user = await _auth.currentUser();
    DocumentReference ref = await Firestore.instance.collection("users").document(user.uid);
    DocumentSnapshot snap = await ref.get();
    currentuser = User(await snap.data.entries.elementAt(3).value, await snap.data.entries.elementAt(2).value, await snap.data.entries.elementAt(0).value, await snap.data.entries.elementAt(1).value,);
  }
}

class MyStatefulWidget extends StatelessWidget {
  const MyStatefulWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          FlatButton.icon(
            color: Colors.blue[200],
            icon: Icon(Icons.exit_to_app), //`Icon` to display
            label: Text('Logout'), //`Text` to display
            onPressed: () {
              BlocProvider.of<AuthenticationBloc>(context).dispatch(
                LoggedOut(),
              );
            },
          )
        ],
      )),
    );
  }
}
